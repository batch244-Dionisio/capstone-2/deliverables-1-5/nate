const express = require ("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const addToCartRoutes = require("./routes/addToCartRoutes");

const app = express()

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.z2a08wk.mongodb.net/capstone_booking?retryWrites=true&w=majority", 
{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => 
	console.log('Connected to the MongoDB Atlas.'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/cart", addToCartRoutes);


app.listen(process.env.PORT || 3000, () => {
	console.log(`API is running at port ${process.env.PORT || 3000}`)
});