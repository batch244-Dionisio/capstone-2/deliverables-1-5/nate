const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require ("../auth");


// < ---------------------------- SESSION 43 ----------------------------->

// ADD PRODUCT (Admin Only) 
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if (data.isAdmin){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});

// RETRIVE ALL PRODUCTS 
router.get ("/allProducts", (req, res) => {
	productController.getAllproducts().then(resultFromController => res.send (resultFromController));
});

// RETRIVE ACTIVE PRODUCTS 
router.get("/active", (req, res) => {
	productController.allActiveProduct().then(resultFromController => res.send (resultFromController));
});




// < ---------------------------- SESSION 44 ----------------------------->

// RETRIVE SPECIFIC PRODUCT 
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController));
});

// Update Product (Admin Only) 
router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});

// ARCHIVE A PRODUCT (Admin Only) 
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});
 

module.exports = router