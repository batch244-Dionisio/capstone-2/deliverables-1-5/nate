const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require ("../auth");


// < ---------------------------- SESSION 42 ----------------------------->

// USER REGISTRATION 
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController));
});

// USER AUTHENTICATION 
router.post("/login", (req,	res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});



// < ---------------------------- SESSION 44 ----------------------------->

// NON-ADMIN USER CHECKOUT  
router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId :  userData.id,
		productId : req.body.productId,
		quantity: req.body.quantity
	}
	userController.checkout(data).then((resultFromController) => res.send(resultFromController));
});


// USER DETAILS    
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.userDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



// < ---------------------------- STRETCH GOAL!! ----------------------------->

// SET USER AS ADMIN (Admin Only)
router.put("/:userId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
	userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});

// RETRIEVE ALL ORDERS (Non Admin)
router.get ("/myOrder", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	if (!data.isAdmin) {
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});

// RETRIEVE ALL ORDERS (Admin Only)
router.get ("/allOrders", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
	userController.totalOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});


module.exports = router;