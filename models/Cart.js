const mongoose = require("mongoose");

const AddToCartSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: true
		}, 
		orders: [
			{
				products: [
				 	{
				 	productName: {
				 		type: String,
				 		required: [true, "Product Name is required."]
				 	},
				 	productId: {
				 		type: String,
				 		required: [true, "Product Name is required."]
				 	},
				 	quantity: {
				 		type: Number,
					 	required: [true, "Quantity is required."]
				 	},
				 	isActive: {
		 				type: Boolean,
		 				default: true
		 			}
				 }	 					
				], 
				totalAmount: {
					type: Number,
					required: [true, "Total Amount is required."]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
)

module.exports = mongoose.model("addToCart", AddToCartSchema);
