const bcrypt = require("bcrypt");
const auth = require ("../auth");
const User = require("../models/User");
const Product = require("../models/Product");


// < ---------------------------- SESSION 42 ----------------------------->

// USER REGISTRATION 
module.exports.registerUser = (reqBody) =>  {

	let newUser = new User ({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

// USER AUTHENTICATION 
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {acess: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};



// < ---------------------------- SESSION 45 ----------------------------->

// NON-ADMIN USER CHECKOUT 
module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(async user => {
		if(!user.isAdmin) {
			let products = []
			console.log(products)
			let product = await Product.findById(data.productId).then(product => {
				return product
			})

	   		const totalAmount = product.price * data.quantity

			products.push({
				productName: product.name,
				quantity: data.quantity
			})

			console.log(products)

			user.orders.push({products, totalAmount, purchasedOn: new Date()});
			return user.save().then((user, error) => {
				if (error){
					return false;
				} else {
					return true;
				}
			})
		} else {
			return false;
		}	
	});
	 return  isUserUpdated
}



// USER DETAILS  
module.exports.userDetails =(reqBody) => {
	return User.findById(reqBody.userId).then(result => {
		return result;
	});
	
};	



// < ---------------------------- STRETCH GOAL!! ----------------------------->

// SET USER AS ADMIN (Admin Only) 
module.exports.updateUser = (reqParams, reqBody) => {

	let updatedUser = {
		isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
		if (error) {
			return false;
		} else {
			return true
		}
	})
};

// RETRIEVE ALL ORDERS (Non Admin) 
module.exports.getAllOrders = (data) => {
	return User.findById(data.id).then((user)=> {
		return user.orders
	})
}


// RETRIEVE ALL ORDERS (Admin Only) 
module.exports.totalOrders = () => {
	return User.find({isAdmin: false}).then((user)=> 
		{
			let orders = []
			user.forEach(user => {
			orders.push(user.orders)
			})
			return orders
		}
	)
}


