const bcrypt = require("bcrypt");
const auth = require ("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");



// < ---------------------------- ADD TO CART ------------------------------------>


module.exports.addToCart = async (data) => {
	let products = []
	let product = await Product.findById(data.productId).then(product => {
		return product
	})
	const totalAmount = product.price * data.quantity
	products.push({
		productName: product.name,
		quantity: data.quantity,
		isActive: data.isActive,
		productId: data.productId
	})	
	let cart = AddToCart.find({userId: data.userId}).then(cart => {
		if (cart.length) {
			console.log(cart[0].orders[0])

			let isfound= false
			let orderIndex = 0

			for(let index = 0; index < cart[0].orders.length; index++){
				if(cart[0].orders[index].products[0].productId == data.productId){
					orderIndex = index
					isfound = true;
					break
				}

			}

			if(isfound){
				cart[0].orders[orderIndex].products[0].quantity += data.quantity
				const newProductTotalPrice = product.price * data.quantity
				cart[0].orders[orderIndex].totalAmount += newProductTotalPrice
			} else {
				cart[0].orders.push({products, totalAmount, purchasedOn: new Date()})
			}
			
			return cart[0].save().then((_,error)=>{
				if (error){
				return false;
				} else {
					return true;
				}
			})

		} else {
			let orders = []
			orders.push({products, totalAmount, purchasedOn: new Date()});
			let newOrder = new AddToCart({orders: orders, userId: data.userId})
			return newOrder.save().then((order ,error) => {
				if (error){
					return false;
				} else {
					return true;
				}
			})
		}
	})
	return cart
}


// REMOVE PRODUCT FROM CART
module.exports.removeProduct = async(data) => {
	let cart = await AddToCart.find({userId: data.userId}).then(cart => {
		if (cart.length) {
			let isfound= false
			let orderIndex = 0

			for(let index = 0; index < cart[0].orders.length; index++){
				if(cart[0].orders[index].products[0].productId == data.productId){
					orderIndex = index
					isfound = true;
					break
				}
			}

			if(isfound){
				cart[0].orders[orderIndex].products[0].isActive = false
				return cart[0].save().then((_,error)=>{
					if (error){
					return false;
					} else {
						return true;
					}
				})
			} else {
				return false;
			}
		} else {
			return false;
		}
	})
	return cart
};


// TOTAL PRICE FOR ALL ITEMS
module.exports.overAll = async (data) => {
	let cart = await AddToCart.find({userId: data.userId}).then(cart => {
		if (cart.length) {
			console.log(cart[0].orders[0])

			let overallTotal = 0

			for(let index = 0; index < cart[0].orders.length; index++){
				if(cart[0].orders[index].products[0].isActive){
					overallTotal += cart[0].orders[index].totalAmount
				}
			}
			
			console.log()
			return {overAllTotal: overallTotal}
			
		} else {
			return false
		}
	})
	return cart
}