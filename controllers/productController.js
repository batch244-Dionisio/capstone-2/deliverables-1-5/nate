const Product = require("../models/Product");


// < ---------------------------- SESSION 43 ----------------------------->

// ADD PRODUCT (Admin Only) 
module.exports.addProduct = (data) => {

	let newProduct = new Product ({
		name: data.product.name,
		description : data.product.description,
		price : data.product.price
	});

	return newProduct.save().then((product, error) => {
		if (error){
			return false
		} else {
			return true;
		}
	})		
};

// RETRIVE ALL PRODUCTS 
module.exports.getAllproducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// RETRIVE ACTIVE PRODUCTS 
module.exports.allActiveProduct = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}


// < ---------------------------- SESSION 44 ----------------------------->

// RETRIVE SPECIFIC PRODUCT 
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}

// Update Product (Admin Only) 
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true
		}
	})
};

// ARCHIVE A PRODUCT (Admin Only) 
module.exports.archiveProduct = (reqParams,	reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) =>{
		if (error){
			return false;
		} else {
			return true;
		}
	})
};
